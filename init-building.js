const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/room')
const Building = require('./models/building')

async function clearDB () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDB()
  const newInformaticsBuling = new Building({ name: 'New InformaticsBuling', floor: 8 })
  const informaticsBuling = new Building({ name: 'informatics', floor: 11 })
  const room3c01 = new Room({ name: '3c01', capacity: 200, floor: 3, building: informaticsBuling })
  informaticsBuling.room.push(room3c01)
  const room4c01 = new Room({ name: '4c01', capacity: 150, floor: 4, building: informaticsBuling })
  informaticsBuling.room.push(room4c01)
  const room5c01 = new Room({ name: '5c01', capacity: 100, floor: 5, building: informaticsBuling })
  informaticsBuling.room.push(room5c01)
  informaticsBuling.save()
  newInformaticsBuling.save()
  room3c01.save()
  room4c01.save()
  room5c01.save()
}
main().then(function () {
  console.log('finish')
})
