const mongoose = require('mongoose')
const { Schema } = mongoose
const room = Schema({
  name: String,
  capacity: { type: Number, default: 50 },
  floor: Number,
  building: [{ type: Schema.Types.ObjectId, ref: 'Building' }]

})
module.exports = mongoose.model('Room', room)
